
#include "logger.h"

#include <chrono>

#include <spdlog/async.h>
#include <spdlog/details/registry.h>
#include <spdlog/logger.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/syslog_sink.h>
#include <spdlog/sinks/udp_sink.h>

namespace video_streamer
{
namespace log
{
Logger::Logger(LogLevel level)
{
    spdlog::init_thread_pool(8192U, 2U);
    auto stdout_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    auto udpsink = std::make_shared<spdlog::sinks::udp_sink_mt>(
        "192.168.7.10", kLoggerName, 35000);
    auto syslog_sink = std::make_shared<spdlog::sinks::syslog_sink_mt>(kLoggerName);

    mLogSinks = {stdout_sink, syslog_sink, udpsink};

    auto logger = std::make_shared<spdlog::async_logger>(
        kLoggerName, std::cbegin(mLogSinks), std::cend(mLogSinks), spdlog::thread_pool(), spdlog::async_overflow_policy::block);
    spdlog::register_logger(logger);
    setLogLevel(level);
    spdlog::flush_every(std::chrono::seconds(1));
}

Logger::~Logger()
{
    spdlog::shutdown();
}

void Logger::setLogLevel(LogLevel level) const
{
    switch (level) {
        case LogLevel::kTrace:
            spdlog::set_level(spdlog::level::trace);
            break;
        case LogLevel::kDebug:
            spdlog::set_level(spdlog::level::debug);
            break;
        case LogLevel::kInfo:
            spdlog::set_level(spdlog::level::info);
            break;
        case LogLevel::kWarn:
            spdlog::set_level(spdlog::level::warn);
            break;
        case LogLevel::kError:
            spdlog::set_level(spdlog::level::err);
            break;
        default:
            spdlog::set_level(spdlog::level::info);
    }
}

}   // namespace log
}   // namespace video_streamer
