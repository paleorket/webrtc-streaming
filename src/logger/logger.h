
#ifndef SRC_LOG_LOGGER_H_
#define SRC_LOG_LOGGER_H_

#include <memory>
#include <vector>

#include <spdlog/spdlog.h>

#include "../utils/singleton.h"

namespace video_streamer {
namespace log {

static constexpr char kLoggerName[]{"VideoStreamingLog"};

enum class LogLevel : uint8_t { kTrace, kDebug, kInfo, kWarn, kError };

/*!
 * \brief Класс логирования, инкапсулирующий общий интерфейс лога кастомной или
 * библиотечной реализации
 */
class Logger {
public:
  Logger() = delete;
  /*!
   * \brief Конструктор
   */
  explicit Logger(const LogLevel level = LogLevel::kInfo);
  /*!
   * \brief Деструктор
   */
  ~Logger();
  /*!
   * \brief Запрет на конструктор и оператор копирования */
  Logger(const Logger &) = delete;
  Logger &operator=(const Logger &) = delete;

  /*!
   * \brief Низший уровень лога - трассировки
   * \param[in] params Эллипс параметров, рекомендуемый формат: сообщение, имя
   * функции, доп. параметры
   */
  template <typename... Args> void logTrace(Args &&... params) const {
    spdlog::get(kLoggerName)->trace(std::forward<Args>(params)...);
  }
  /*!
   * \brief Уровень дебага
   * \param[in] params Эллипс параметров, рекомендуемый формат: сообщение, имя
   * функции, доп. параметры
   */
  template <typename... Args> void logDebug(Args &&... params) const {
    spdlog::get(kLoggerName)->debug(std::forward<Args>(params)...);
  }
  /*!
   * \brief Уровень информации
   * \param[in] params Эллипс параметров, рекомендуемый формат: сообщение, имя
   * функции, доп. параметры
   */
  template <typename... Args> void logInfo(Args &&... params) const {
    spdlog::get(kLoggerName)->info(std::forward<Args>(params)...);
  }
  /*!
   * \brief Уровень предупреждений
   * \param[in] params Эллипс параметров, рекомендуемый формат: сообщение, имя
   * функции, доп. параметры
   */
  template <typename... Args> void logWarn(Args &&... params) const {
    spdlog::get(kLoggerName)->warn(std::forward<Args>(params)...);
  }
  /*!
   * \brief Уровень ошибок
   * \param[in] params Эллипс параметров, рекомендуемый формат: сообщение, имя
   * функции, доп. параметры
   */
  template <typename... Args> void logError(Args &&... params) const {
    spdlog::get(kLoggerName)->error(std::forward<Args>(params)...);
  }

private:
  /*!
   * \brief Устанавливает глобальный уровень логирования по принятым настройкам
   */
  void setLogLevel(const LogLevel level) const;
  /*!
   * \brief Приемники логирования (консоль, файл и т.д.)
   */
  std::vector<spdlog::sink_ptr> mLogSinks;
};

inline void init(LogLevel level)
{
    utils::Singleton<log::Logger> logger{level};
}

inline void reset()
{
    utils::Singleton<log::Logger>::reset();
}

template<typename... Args>
inline void logTrace(Args &&...params)
{
    utils::Singleton<log::Logger>::get()->logTrace(std::forward<Args>(params)...);
}

template<typename... Args>
inline void logDebug(Args &&...params)
{
    utils::Singleton<log::Logger>::get()->logDebug(std::forward<Args>(params)...);
}

template<typename... Args>
inline void logInfo(Args &&...params)
{
    utils::Singleton<log::Logger>::get()->logInfo(std::forward<Args>(params)...);
}

template<typename... Args>
inline void logWarn(Args &&...params)
{
    utils::Singleton<log::Logger>::get()->logWarn(std::forward<Args>(params)...);
}

template<typename... Args>
inline void logError(Args &&...params)
{
    utils::Singleton<log::Logger>::get()->logError(std::forward<Args>(params)...);
}

} // namespace log
} // namespace video_streamer

#endif // SRC_LOG_LOGGER_H_
