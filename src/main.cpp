
#include <atomic>
#include <csignal>

#include "application.h"

namespace
{
std::atomic_bool gExitRequested{false};

void sigTermHandler(int)
{
    gExitRequested = true;
}
}   // namespace

using namespace video_streamer;

int main()
{
    std::signal(SIGTERM, sigTermHandler);

    Application app;
    app.run([]() { return gExitRequested == true; });

    return 0;
}
