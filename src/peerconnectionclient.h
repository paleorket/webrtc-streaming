#ifndef PEERCONNECTIONCLIENT_H
#define PEERCONNECTIONCLIENT_H

#include <memory>

#include <rtc_base/net_helpers.h>
#include <rtc_base/third_party/sigslot/sigslot.h>
#include <rtc_base/thread.h>
#include <sio_message.h>

namespace video_streamer
{
struct PeerMessage
{
    int peerId;
    sio::message::ptr data;
};

struct PeerConnectionClientObserver
{
    virtual void OnPeerConnected(int id, const std::string &name) = 0;
    virtual void OnPeerDisconnected(int peerId) = 0;
    virtual void OnMessageFromPeer(PeerMessage &&message) = 0;
    virtual void OnServerConnectionFailure() = 0;

protected:
    virtual ~PeerConnectionClientObserver() {}
};

class PeerConnectionClient : public sigslot::has_slots<>,
                             public rtc::MessageHandler
{
public:
    PeerConnectionClient();
    ~PeerConnectionClient() override;

    void RegisterObserver(PeerConnectionClientObserver *callback);
    void Connect(const std::string &server);
    void SendToPeer(PeerMessage &&message);
    void Disconnect();

    // implements the MessageHandler interface
    void OnMessage(rtc::Message *msg) override;

private:
    class Data;
    std::unique_ptr<Data> data;
};

}   // namespace video_streamer

#endif // PEERCONNECTIONCLIENT_H
