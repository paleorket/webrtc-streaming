#include "peerconnectionclient.h"

#include <sio_client.h>
#include <sio_socket.h>

#include "logger/logger.h"

namespace video_streamer
{
class PeerConnectionClient::Data
{
public:
    Data()
        : client{std::make_unique<sio::client>()}
    {
        auto socket = client->socket();
        socket->on("message", [this](const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp) {
            OnMessageFromPeer(name, data, hasAck, ackResp);
        });
        socket->on("join", [this](const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp) {
            OnRoomConnection(name, data, hasAck, ackResp);
        });
        socket->on("joined", [this](const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp) {
            OnRoomConnected(name, data, hasAck, ackResp);
        });
        socket->on("full", [this](const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp) {
            OnServerConnectionFailure(name, data, hasAck, ackResp);
        });

        client->set_socket_open_listener([this](const std::string &nsp) { OnConnected(nsp); });
        client->set_close_listener([this](const sio::client::close_reason &reason) { OnClosed(reason); });
        client->set_fail_listener([this]() { OnFailed(); });
    }

    void SendMessage(PeerMessage &&message);
    void DoConnect(const std::string &url);
    void Close();

    // Callback handlers for socket io
    void OnRoomConnected(const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp);
    void OnRoomConnection(const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp);
    void OnMessageFromPeer(const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp);
    void OnServerConnectionFailure(const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp);
    void OnConnected(const std::string &nsp);
    void OnClosed(const sio::client::close_reason &reason);
    void OnFailed();

    std::unique_ptr<sio::client> client;
    PeerConnectionClientObserver *callback{nullptr};
    int peerId{1};
};

}   // namespace video_streamer

namespace
{
constexpr char kSessionHangUp[]{"bye"};
}

using namespace video_streamer;

// PeerConnectionClient::Data
void PeerConnectionClient::Data::DoConnect(const std::string &url)
{
    if (!url.empty()) {
        client->connect(url);
    }
}

void PeerConnectionClient::Data::Close()
{
    if (client->opened()) {
        client->sync_close();
    }
}

void PeerConnectionClient::Data::SendMessage(PeerMessage &&message)
{
    client->socket()->emit("message", std::move(message.data));
}

// Callback handlers for socket io
void PeerConnectionClient::Data::OnRoomConnected(const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp)
{
    static_cast<void>(name);
    static_cast<void>(hasAck);
    static_cast<void>(ackResp);
    static_cast<void>(data);

    log::logDebug("PeerConnectionClient::{0}", __func__);
}

void PeerConnectionClient::Data::OnRoomConnection(const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp)
{
    static_cast<void>(name);
    static_cast<void>(hasAck);
    static_cast<void>(ackResp);
    static_cast<void>(data);

    log::logDebug("PeerConnectionClient::{0}", __func__);
}

void PeerConnectionClient::Data::OnMessageFromPeer(const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp)
{
    static_cast<void>(name);
    static_cast<void>(hasAck);
    static_cast<void>(ackResp);

    if (data->get_flag() == sio::message::flag_string) {
        auto msg = data->get_string();
        if (msg == std::string(kSessionHangUp)) {
            callback->OnPeerDisconnected(peerId);
        }
    }
    else if (data->get_flag() == sio::message::flag_object) {
        callback->OnMessageFromPeer({peerId, data});
    }
}

void PeerConnectionClient::Data::OnServerConnectionFailure(const std::string &name, const sio::message::ptr &data, bool hasAck, sio::message::list &ackResp)
{
    static_cast<void>(name);
    static_cast<void>(data);
    static_cast<void>(hasAck);
    static_cast<void>(ackResp);

    log::logWarn("PeerConnectionClient::{0}: Failed to connect to server, peer: {1}", __func__, peerId);
    callback->OnPeerDisconnected(peerId);
}

void PeerConnectionClient::Data::OnConnected(const std::string &nsp)
{
    log::logDebug("PeerConnectionClient::{0}: nsp: {1}", __func__, nsp);
    callback->OnPeerConnected(peerId, {});
}

void PeerConnectionClient::Data::OnClosed(const sio::client::close_reason &reason)
{
    static_cast<void>(reason);

    callback->OnPeerDisconnected(peerId);
}

void PeerConnectionClient::Data::OnFailed()
{
    log::logDebug("PeerConnectionClient::{0}", __func__);

    callback->OnServerConnectionFailure();
}

// PeerConnectionClient
PeerConnectionClient::PeerConnectionClient()
    : data{std::make_unique<Data>()}
{
}

PeerConnectionClient::~PeerConnectionClient()
{
    Disconnect();
}

void PeerConnectionClient::RegisterObserver(PeerConnectionClientObserver *callback)
{
    data->callback = callback;
}

void PeerConnectionClient::Connect(const std::string &server)
{
    data->DoConnect(server);
}

void PeerConnectionClient::SendToPeer(PeerMessage &&message)
{
    data->SendMessage(std::move(message));
}

void PeerConnectionClient::Disconnect()
{
    data->Close();
}

void PeerConnectionClient::OnMessage(rtc::Message *msg)
{
    static_cast<void>(msg);
}
