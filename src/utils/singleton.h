#ifndef SRC_UTILS_SINGLETON_H_
#define SRC_UTILS_SINGLETON_H_

#include <atomic>
#include <memory>
#include <mutex>
#include <type_traits>

namespace utils
{
/**
 * \brief Singleton with thread-safe initialization
 * \tparam InstanceType Type of class to be encapsulated as singleton
 */
template<typename InstanceType>
class Singleton
{
public:
    /**
   * \brief Constructor
   */
    template<typename... Args>
    Singleton(Args &&...params)
    {
        std::call_once(mFirstInitFlag, onceInit);
        std::call_once(*mInitFlag, [&params...]() {
            mInstance = std::make_shared<InstanceType>(std::forward<Args>(params)...);
        });
    }

    Singleton(const Singleton &other) = delete;
    Singleton &operator=(const Singleton &other) = delete;
    Singleton(Singleton &&other) = delete;
    Singleton &operator=(Singleton &&other) = delete;

    /*!
   * \brief   Reset call once flag
   * \details This method can call before constructor. This method is
   * thread-safe.
   */
    static void reset()
    {
        if (mResetFlag.exchange(false)) {
            delete mInitFlag;
            mInitFlag = nullptr;
            mInitFlag = new std::once_flag();
            mInstance = std::shared_ptr<InstanceType>();
            mResetFlag.store(true);
        }
    }

    /*!
   * \brief   Returns an shareable pointer to the corresponding type
   * InstanceType. 
   * \return  std::shared_ptr<InstanceType>
   */
    static std::shared_ptr<InstanceType> get()
    {
        return mInstance;
    }

protected:
    /*!
   * \brief   First initialization this class in application
   */
    static void onceInit()
    {
        mInitFlag = new std::once_flag();
        mResetFlag.store(true);
    }

    static std::shared_ptr<InstanceType> mInstance;
    static std::once_flag mFirstInitFlag;
    static std::once_flag *mInitFlag;
    static std::atomic_bool mResetFlag;
};

template<typename InstanceType>
std::shared_ptr<InstanceType> Singleton<InstanceType>::mInstance{};

template<typename InstanceType>
std::once_flag
    Singleton<InstanceType>::mFirstInitFlag{};   //!< Once in application
                                                 //!< initialization flag

template<typename InstanceType>
std::once_flag *Singleton<InstanceType>::mInitFlag{
    nullptr};   //!< Flag for call once with reset opportunity

template<typename InstanceType>
std::atomic_bool
    Singleton<InstanceType>::mResetFlag{};   //!< Flag for thread-safe
                                             //!< reset HandlerStorage

}   // namespace utils

#endif   // SRC_UTILS_SINGLETON_H_
