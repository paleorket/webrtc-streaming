#ifndef VIDEOSINK_H
#define VIDEOSINK_H

#include <api/media_stream_interface.h>
#include <api/video/video_frame.h>
#include <api/video/video_sink_interface.h>

namespace video_streamer
{
class VideoSink : public rtc::VideoSinkInterface<webrtc::VideoFrame>
{
public:
    explicit VideoSink(webrtc::VideoTrackInterface *trackToRender);

    virtual ~VideoSink() override;

    // VideoSinkInterface implementation
    void OnFrame(const webrtc::VideoFrame &frame) override;

protected:
    rtc::scoped_refptr<webrtc::VideoTrackInterface> mRenderedTrack;

private:
    int64_t mFirstFrameTimestamp;
};

}   // namespace video_streamer

#endif // VIDEOSINK_H
