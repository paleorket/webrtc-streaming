#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <memory>

namespace video_streamer
{
class Controller
{
public:
    Controller();
    ~Controller();

    void startStreaming();

    void stopStreaming();

private:
    class Data;
    std::unique_ptr<Data> data;
};

}   // namespace video_streamer
#endif   // CONTROLLER_H
