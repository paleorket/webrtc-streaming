#include "videocapturer.h"

#include <algorithm>

#include <api/scoped_refptr.h>
#include <api/video/i420_buffer.h>
#include <api/video/video_frame_buffer.h>
#include <api/video/video_rotation.h>
#include <modules/video_capture/video_capture_factory.h>
#include <rtc_base/checks.h>

#include "logger/logger.h"

namespace video_streamer
{
// VideoCapturer
VideoCapturer::~VideoCapturer() = default;

void VideoCapturer::OnFrame(const webrtc::VideoFrame &originalFrame)
{
    int croppedWidth = 0;
    int croppedHeight = 0;
    int outWidth = 0;
    int outHeight = 0;

    webrtc::VideoFrame frame = MaybePreprocess(originalFrame);

    if (!mVideoAdapter.AdaptFrameResolution(
            frame.width(), frame.height(), frame.timestamp_us() * 1000, &croppedWidth, &croppedHeight, &outWidth, &outHeight)) {
        // Drop frame in order to respect frame rate constraint.
        return;
    }

    if (outHeight != frame.height() || outWidth != frame.width()) {
        // Video adapter has requested a down-scale. Allocate a new buffer and
        // return scaled version.
        // For simplicity, only scale here without cropping.
        rtc::scoped_refptr<webrtc::I420Buffer> scaledBuffer = webrtc::I420Buffer::Create(outWidth, outHeight);
        scaledBuffer->ScaleFrom(*frame.video_frame_buffer()->ToI420());
        webrtc::VideoFrame::Builder newFrameBuilder = webrtc::VideoFrame::Builder()
                                                          .set_video_frame_buffer(scaledBuffer)
                                                          .set_rotation(webrtc::kVideoRotation_0)
                                                          .set_timestamp_us(frame.timestamp_us())
                                                          .set_id(frame.id());
        if (frame.has_update_rect()) {
            absl::optional<webrtc::VideoFrame::UpdateRect> newRect = frame.update_rect().ScaleWithFrame(
                frame.width(), frame.height(), 0, 0, frame.width(), frame.height(), outWidth, outHeight);
            newFrameBuilder.set_update_rect(newRect);
        }
        mBroadcaster.OnFrame(newFrameBuilder.build());
    }
    else {
        // No adaptations needed, just return the frame as is.
        mBroadcaster.OnFrame(frame);
    }
}

rtc::VideoSinkWants VideoCapturer::GetSinkWants()
{
    return mBroadcaster.wants();
}

void VideoCapturer::AddOrUpdateSink(
    rtc::VideoSinkInterface<webrtc::VideoFrame> *sink,
    const rtc::VideoSinkWants &wants)
{
    mBroadcaster.AddOrUpdateSink(sink, wants);
    UpdateVideoAdapter();
}

void VideoCapturer::RemoveSink(rtc::VideoSinkInterface<webrtc::VideoFrame> *sink)
{
    mBroadcaster.RemoveSink(sink);
    UpdateVideoAdapter();
}

void VideoCapturer::UpdateVideoAdapter()
{
    mVideoAdapter.OnSinkWants(mBroadcaster.wants());
}

webrtc::VideoFrame VideoCapturer::MaybePreprocess(const webrtc::VideoFrame &frame)
{
    webrtc::MutexLock lock(&mLock);
    if (mPreprocessor != nullptr) {
        return mPreprocessor->Preprocess(frame);
    }
    else {
        return frame;
    }
}

// VcmCapturer
VcmCapturer::VcmCapturer()
    : mVcm(nullptr)
{}

bool VcmCapturer::Init(size_t width,
                       size_t height,
                       size_t targetFps,
                       size_t captureDeviceIndex)
{
    std::unique_ptr<webrtc::VideoCaptureModule::DeviceInfo> deviceInfo(
        webrtc::VideoCaptureFactory::CreateDeviceInfo());

    char deviceName[256];
    char uniqueName[256];
    if (deviceInfo->GetDeviceName(static_cast<uint32_t>(captureDeviceIndex),
                                  deviceName,
                                  sizeof(deviceName),
                                  uniqueName,
                                  sizeof(uniqueName))
        != 0) {
        Destroy();
        return false;
    }

    mVcm = webrtc::VideoCaptureFactory::Create(uniqueName);
    if (!mVcm) {
        return false;
    }
    mVcm->RegisterCaptureDataCallback(this);

    deviceInfo->GetCapability(mVcm->CurrentDeviceName(), 0, mCapability);

    mCapability.width = static_cast<int32_t>(width);
    mCapability.height = static_cast<int32_t>(height);
    mCapability.maxFPS = static_cast<int32_t>(targetFps);
    mCapability.videoType = webrtc::VideoType::kI420;

    if (mVcm->StartCapture(mCapability) != 0) {
        Destroy();
        return false;
    }

    RTC_CHECK(mVcm->CaptureStarted());

    return true;
}

VcmCapturer *VcmCapturer::Create(size_t width,
                                 size_t height,
                                 size_t targetFps,
                                 size_t captureDeviceIndex)
{
    std::unique_ptr<VcmCapturer> vcm_capturer(new VcmCapturer());
    if (!vcm_capturer->Init(width, height, targetFps, captureDeviceIndex)) {
        log::logWarn("{0}: Failed to create VcmCapturer(w = {1}, h = {2}, fps = {3})", __func__, width, height, targetFps);
        return nullptr;
    }
    return vcm_capturer.release();
}

void VcmCapturer::Destroy()
{
    if (!mVcm)
        return;

    mVcm->StopCapture();
    mVcm->DeRegisterCaptureDataCallback();
    // Release reference to VCM.
    mVcm = nullptr;
}

VcmCapturer::~VcmCapturer()
{
    Destroy();
}

void VcmCapturer::OnFrame(const webrtc::VideoFrame &frame)
{
    VideoCapturer::OnFrame(frame);
}

}   // namespace video_streamer
