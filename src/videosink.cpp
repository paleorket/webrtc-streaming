#include "videosink.h"

#include <chrono>

#include <api/video/i420_buffer.h>

namespace video_streamer
{
VideoSink::VideoSink(webrtc::VideoTrackInterface *trackToRender)
    : mRenderedTrack{trackToRender}
    , mFirstFrameTimestamp{-1}
{
    mRenderedTrack->AddOrUpdateSink(this, rtc::VideoSinkWants());
}

VideoSink::~VideoSink()
{
    mRenderedTrack->RemoveSink(this);
}

void VideoSink::OnFrame(const webrtc::VideoFrame &videoFrame)
{
    int64_t videoFramePts{videoFrame.timestamp_us()};
    int64_t pts{-1};
    if (mFirstFrameTimestamp == -1) {
        int64_t now = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now().time_since_epoch())
                          .count();
        pts = mFirstFrameTimestamp = now - videoFramePts;
    }
    else {
        pts = mFirstFrameTimestamp + videoFramePts;
    }
    rtc::scoped_refptr<webrtc::I420BufferInterface> buffer{videoFrame.video_frame_buffer()->ToI420()};
    if (videoFrame.rotation() != webrtc::kVideoRotation_0) {
        buffer = webrtc::I420Buffer::Rotate(*buffer, videoFrame.rotation());
    }
}

}   // namespace video_streamer
