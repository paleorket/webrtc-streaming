#ifndef VIDEOCAPTURER_H
#define VIDEOCAPTURER_H

#include <api/scoped_refptr.h>
#include <api/video/video_frame.h>
#include <api/video/video_source_interface.h>
#include <media/base/video_adapter.h>
#include <media/base/video_broadcaster.h>
#include <modules/video_capture/video_capture.h>
#include <rtc_base/synchronization/mutex.h>

namespace video_streamer
{
class VideoCapturer : public rtc::VideoSourceInterface<webrtc::VideoFrame>
{
public:
    class FramePreprocessor
    {
    public:
        virtual ~FramePreprocessor() = default;

        virtual webrtc::VideoFrame Preprocess(const webrtc::VideoFrame &frame) = 0;
    };

    ~VideoCapturer() override;

    void AddOrUpdateSink(rtc::VideoSinkInterface<webrtc::VideoFrame> *sink,
                         const rtc::VideoSinkWants &wants) override;
    void RemoveSink(rtc::VideoSinkInterface<webrtc::VideoFrame> *sink) override;
    void SetFramePreprocessor(std::unique_ptr<FramePreprocessor> preprocessor)
    {
        webrtc::MutexLock lock(&mLock);
        mPreprocessor = std::move(preprocessor);
    }

protected:
    void OnFrame(const webrtc::VideoFrame &frame);
    rtc::VideoSinkWants GetSinkWants();

private:
    void UpdateVideoAdapter();
    webrtc::VideoFrame MaybePreprocess(const webrtc::VideoFrame &frame);

    webrtc::Mutex mLock;
    std::unique_ptr<FramePreprocessor> mPreprocessor RTC_GUARDED_BY(mLock);
    rtc::VideoBroadcaster mBroadcaster;
    cricket::VideoAdapter mVideoAdapter;
};

class VcmCapturer : public VideoCapturer,
                    public rtc::VideoSinkInterface<webrtc::VideoFrame>
{
public:
    static VcmCapturer *Create(size_t width,
                               size_t height,
                               size_t targetFps,
                               size_t captureDeviceIndex);
    virtual ~VcmCapturer();

    void OnFrame(const webrtc::VideoFrame &frame) override;

private:
    VcmCapturer();
    bool Init(size_t width,
              size_t height,
              size_t targetFps,
              size_t captureDeviceIndex);
    void Destroy();

    rtc::scoped_refptr<webrtc::VideoCaptureModule> mVcm;
    webrtc::VideoCaptureCapability mCapability;
};

}   // namespace video_streamer

#endif // VIDEOCAPTURER_H
