#include "controller.h"

#include <functional>

#include <api/audio_codecs/audio_decoder_factory_template.h>
#include <api/audio_codecs/builtin_audio_encoder_factory.h>
#include <api/audio_codecs/builtin_audio_decoder_factory.h>
#include <api/create_peerconnection_factory.h>
#include <api/jsep.h>
#include <api/media_stream_interface.h>
#include <api/peer_connection_interface.h>
#include <api/peer_connection_factory_proxy.h>
#include <api/video_codecs/builtin_video_decoder_factory.h>
#include <api/video_codecs/builtin_video_encoder_factory.h>
#include <modules/audio_device/include/audio_device.h>
#include <modules/audio_processing/include/audio_processing.h>
#include <modules/video_capture/video_capture.h>
#include <modules/video_capture/video_capture_factory.h>
#include <pc/video_track_source.h>
#include <rapidjson/allocators.h>
#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/rapidjson.h>
#include <rapidjson/stringbuffer.h>
#include <rtc_base/checks.h>
#include <rtc_base/thread.h>

#include "logger/logger.h"
#include "peerconnectionclient.h"
#include "videocapturer.h"
#include "videosink.h"

namespace video_streamer
{
class Controller::Data : public webrtc::PeerConnectionObserver,
                         public webrtc::CreateSessionDescriptionObserver,
                         public PeerConnectionClientObserver
{
public:
    Data()
        : mClient{
            std::make_unique<PeerConnectionClient>()}
    {
        mClient->RegisterObserver(this);
    }

    ~Data()
    {
        DeletePeerConnection();
    }

    void Connect(const std::string &server);

    void Disconnect();

    bool InitializePeerConnection();

    bool CreatePeerConnection(bool dtls);

    void DeletePeerConnection();

    void AddTracks();

    //
    // PeerConnectionClientObserver
    //
    void OnPeerConnected(int id, const std::string &name) override;
    void OnPeerDisconnected(int peerId) override;
    void OnServerConnectionFailure() override;
    void OnMessageFromPeer(PeerMessage &&message) override;

    //
    // PeerConnectionObserver implementation.
    //
    void OnSignalingChange(webrtc::PeerConnectionInterface::SignalingState) override{};
    void OnAddTrack(rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver,
                    const std::vector<rtc::scoped_refptr<webrtc::MediaStreamInterface>> &streams) override;
    void OnRemoveTrack(rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver) override;
    void
        OnDataChannel(rtc::scoped_refptr<webrtc::DataChannelInterface>) override
    {
    }
    void
    OnRenegotiationNeeded() override
    {
    }
    void OnIceConnectionChange(webrtc::PeerConnectionInterface::IceConnectionState) override{};
    void OnIceGatheringChange(webrtc::PeerConnectionInterface::IceGatheringState) override{};
    void OnIceCandidate(const webrtc::IceCandidateInterface *candidate) override;
    void
    OnIceConnectionReceivingChange(bool) override
    {
    }

    // CreateSessionDescriptionObserver implementation.
    void OnSuccess(webrtc::SessionDescriptionInterface *desc) override;
    void OnFailure(webrtc::RTCError error) override;

    void
    AddRef() const override
    {
    }
    rtc::RefCountReleaseStatus
    Release() const override
    {
        return rtc::RefCountReleaseStatus::kDroppedLastRef;
    }

private:
    std::unique_ptr<PeerConnectionClient> mClient;
    std::unique_ptr<rtc::Thread> mSignalingThread;
    std::unique_ptr<rtc::Thread> mWorkerThread;
    std::unique_ptr<VideoSink> mVideoSink;
    rtc::scoped_refptr<webrtc::PeerConnectionInterface> mPeerConnection;
    rtc::scoped_refptr<webrtc::PeerConnectionFactoryInterface> mPeerConnectionFactory;
    int mPeerId;
};
}   // namespace video_streamer

using namespace video_streamer;

namespace
{
constexpr char kServerUrl[] = "http://192.168.0.243:8888";

// Names used for a IceCandidate JSON object.
constexpr char kCandidateSdpMidName[] = "sdpMid";
constexpr char kCandidateSdpMlineIndexName[] = "sdpMLineIndex";
constexpr char kCandidateSdpName[] = "candidate";

constexpr char kSdpOfferName[] = "offer";
constexpr char kSdpAnswerName[] = "answer";

// Names used for a SessionDescription JSON object.
constexpr char kSessionDescriptionTypeName[] = "type";
constexpr char kSessionDescriptionSdpName[] = "sdp";

constexpr char kVideoLabel[] = "video_label";
constexpr char kAudioLabel[] = "audio_label";
constexpr char kStreamId[] = "stream_id";

constexpr char kStunServer[] = "stun:stun4.l.google.com:19302";

class DummySetSessionDescriptionObserver : public webrtc::SetSessionDescriptionObserver
{
public:
    static DummySetSessionDescriptionObserver *
    Create()
    {
        return new rtc::RefCountedObject<DummySetSessionDescriptionObserver>();
    }
    virtual void
    OnSuccess()
    {
        log::logDebug(__PRETTY_FUNCTION__);
    }
    virtual void
    OnFailure(webrtc::RTCError error)
    {
        log::logDebug("{0}: {1}-{2}", __PRETTY_FUNCTION__, ToString(error.type()), error.message());
    }
};

class CapturerTrackSource : public webrtc::VideoTrackSource
{
public:
    static rtc::scoped_refptr<CapturerTrackSource> Create()
    {
        const size_t kWidth = 640;
        const size_t kHeight = 480;
        const size_t kFps = 30;
        std::unique_ptr<video_streamer::VcmCapturer> capturer;
        std::unique_ptr<webrtc::VideoCaptureModule::DeviceInfo> info(
            webrtc::VideoCaptureFactory::CreateDeviceInfo());
        if (!info) {
            return nullptr;
        }
        int numDevices = info->NumberOfDevices();
        for (int i = 0; i < numDevices; ++i) {
            capturer = absl::WrapUnique(
                video_streamer::VcmCapturer::Create(kWidth, kHeight, kFps, i));
            if (capturer) {
                return new rtc::RefCountedObject<CapturerTrackSource>(
                    std::move(capturer));
            }
        }

        return nullptr;
    }

protected:
    explicit CapturerTrackSource(
        std::unique_ptr<video_streamer::VcmCapturer> capturer)
        : VideoTrackSource(/*remote=*/false)
        , mCapturer(std::move(capturer))
    {}

private:
    rtc::VideoSourceInterface<webrtc::VideoFrame> *source() override
    {
        return mCapturer.get();
    }
    std::unique_ptr<video_streamer::VcmCapturer> mCapturer;
};
}   // namespace

void Controller::Data::Connect(const std::string &server)
{
    mClient->Connect(server);
}

void Controller::Data::Disconnect()
{
    mClient->Disconnect();
}

bool Controller::Data::InitializePeerConnection()
{
    bool result{false};

    RTC_DCHECK(!mPeerConnectionFactory);
    RTC_DCHECK(!mPeerConnection);

    mSignalingThread = rtc::Thread::Create();
    mWorkerThread = rtc::Thread::Create();

    mWorkerThread->SetName("worker_thread", nullptr);
    mSignalingThread->SetName("signaling_thread", nullptr);

    if (!mSignalingThread->Start() || !mWorkerThread->Start()) {
        log::logError("{0}: Could not start threads for PeerConnectionFactory!", __func__);
        return result;
    }

    mPeerConnectionFactory = webrtc::CreatePeerConnectionFactory(
        nullptr /* network_thread */, mWorkerThread.get() /* worker_thread */, mSignalingThread.get() /* signaling_thread */, nullptr /* default_adm */, webrtc::CreateBuiltinAudioEncoderFactory(), webrtc::CreateBuiltinAudioDecoderFactory(), webrtc::CreateBuiltinVideoEncoderFactory(), webrtc::CreateBuiltinVideoDecoderFactory(), nullptr /* audio_mixer */, nullptr /* audio_processing */);

    if (!mPeerConnectionFactory) {
        return result;
    }

    if (!CreatePeerConnection(/*dtls=*/true)) {
        DeletePeerConnection();
        return result;
    }

    AddTracks();

    result = mPeerConnection != nullptr;

    return result;
}

bool Controller::Data::CreatePeerConnection(bool dtls)
{
    RTC_DCHECK(mPeerConnectionFactory);
    RTC_DCHECK(!mPeerConnection);

    webrtc::PeerConnectionInterface::RTCConfiguration config;
    config.sdp_semantics = webrtc::SdpSemantics::kUnifiedPlan;
    config.enable_dtls_srtp = dtls;
    config.set_dscp(false);
    config.prune_turn_ports = true;
    //    config.tcp_candidate_policy = webrtc::PeerConnectionInterface::kTcpCandidatePolicyDisabled;
    webrtc::PeerConnectionInterface::IceServer stunServer;
    stunServer.uri = kStunServer;
    config.servers.emplace_back(std::move(stunServer));

    mPeerConnection = mPeerConnectionFactory->CreatePeerConnection(config, nullptr, nullptr, this);
    return mPeerConnection != nullptr;
}

void Controller::Data::DeletePeerConnection()
{
    mPeerConnection->Close();
    mPeerConnection = nullptr;

    mPeerConnectionFactory = nullptr;

    mSignalingThread->Quit();
    mSignalingThread.reset();
    mWorkerThread->Quit();
    mWorkerThread.reset();
}

void Controller::Data::AddTracks()
{
    if (!mPeerConnection->GetSenders().empty()) {
        log::logError("{0}: PeerConnection senders list is empty!", __func__);
        return;
    }

    rtc::scoped_refptr<webrtc::AudioTrackInterface> audioTrack(
        mPeerConnectionFactory->CreateAudioTrack(
            kAudioLabel, mPeerConnectionFactory->CreateAudioSource(cricket::AudioOptions())));
    auto resultOrError = mPeerConnection->AddTrack(audioTrack, {kStreamId});
    if (!resultOrError.ok()) {
        log::logError("{0}: Failed to add audio track to PeerConnection: {1}", __func__, resultOrError.error().message());
    }

    rtc::scoped_refptr<CapturerTrackSource> videoDevice = CapturerTrackSource::Create();
    if (videoDevice) {
        rtc::scoped_refptr<webrtc::VideoTrackInterface> videoTrack(
            mPeerConnectionFactory->CreateVideoTrack(kVideoLabel, videoDevice));

        resultOrError = mPeerConnection->AddTrack(videoTrack, {kStreamId});
        if (!resultOrError.ok()) {
            log::logError("{0}: Failed to add video track to PeerConnection: {1}", __func__, resultOrError.error().message());
        }
    }
    else {
        log::logError("{0}: OpenVideoCaptureDevice failed", __func__);
    }
}

//
// PeerConnectionClientObserver
//
void Controller::Data::OnPeerConnected(int peerId, const std::string &)
{
    mPeerId = peerId;
    InitializePeerConnection();
}

void Controller::Data::OnPeerDisconnected(int)
{
    DeletePeerConnection();
}

void Controller::Data::OnServerConnectionFailure()
{
    DeletePeerConnection();
}

void Controller::Data::OnMessageFromPeer(PeerMessage &&message)
{
    if (!mPeerConnection || !mPeerConnectionFactory) {
        if (!InitializePeerConnection()) {
            log::logError("{0}: Failed to initialize PeerConnection instance", __func__);
        }
    }
    auto data = message.data;
    try {
        std::string typeStr;
        if (data->get_flag() == sio::message::flag_object) {
            typeStr = data->get_map()[kSessionDescriptionTypeName]->get_string();
        }

        // OFFER
        if (typeStr == kSdpOfferName) {
            absl::optional<webrtc::SdpType> sdp_type = webrtc::SdpTypeFromString(typeStr);
            if (!sdp_type) {
                log::logError("{0}: Unknown SDP type: {1}", __func__, typeStr);
                return;
            }
            webrtc::SdpType type = *sdp_type;
            std::string sdp = data->get_map()[kSessionDescriptionSdpName]->get_string();
            if (sdp.empty()) {
                log::logWarn("{0}: Can't parse received session description message.", __func__);
                return;
            }

            log::logDebug("{0}: SDP Offer: {1}", __func__, sdp);

            webrtc::SdpParseError error;
            std::unique_ptr<webrtc::SessionDescriptionInterface>
                remoteSessionDescription = webrtc::CreateSessionDescription(type, sdp, &error);
            if (!remoteSessionDescription) {
                log::logWarn("{0}: Can't parse received session description message. SdpParseError was: {1}", __func__, error.description);
                return;
            }

            mPeerConnection->SetRemoteDescription(
                DummySetSessionDescriptionObserver::Create(),
                remoteSessionDescription.release());

            if (type == webrtc::SdpType::kOffer) {
                mPeerConnection->CreateAnswer(
                    this, webrtc::PeerConnectionInterface::RTCOfferAnswerOptions());
            }
        }   // OFFER
        // ICE CANDIDATE
        else if (typeStr == kCandidateSdpName) {
            std::string sdpMid{"0"};
            int sdpMlineindex{0};
            std::string sdp = data->get_map()[kCandidateSdpName]->get_string();
            if (sdp.empty()) {
                log::logWarn("{0}: Can't parse received session description message.", __func__);
                return;
            }

            log::logDebug("{0}: SDP ICE Candidate: {1}", __func__, sdp);

            webrtc::SdpParseError error;
            std::unique_ptr<webrtc::IceCandidateInterface> candidate(
                webrtc::CreateIceCandidate(sdpMid, sdpMlineindex, sdp, &error));

            if (!candidate.get()) {
                log::logWarn("{0}: Can't parse received candidate message. SdpParseError was: {1}", __func__, error.description);
                return;
            }

            if (!mPeerConnection->AddIceCandidate(candidate.get())) {
                log::logWarn("{0}: Failed to apply the received candidate", __func__);
                return;
            }
        }   // ICE CANDIDATE
    }
    catch (const std::exception &ex) {
        log::logError("{0}: exception {1}", __func__, ex.what());
    }
}

//
// PeerConnectionObserver implementation.
//
void Controller::Data::OnAddTrack(rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver,
                                  const std::vector<rtc::scoped_refptr<webrtc::MediaStreamInterface>> &)
{
    if (!mPeerConnection->GetSenders().empty()) {
        log::logError("{0}: PeerConnection senders list is empty!", __func__);
        return;
    }

    rtc::scoped_refptr<webrtc::AudioTrackInterface> audioTrack(mPeerConnectionFactory->CreateAudioTrack(
        kAudioLabel, mPeerConnectionFactory->CreateAudioSource(cricket::AudioOptions())));
    auto resultOrError = mPeerConnection->AddTrack(audioTrack, {kStreamId});
    if (!resultOrError.ok()) {
        log::logError("{0}: Failed to add audio track to PeerConnection: {1}", __func__, resultOrError.error().message());
    }

    auto *track = receiver->track().release();
    if (track->kind() == webrtc::MediaStreamTrackInterface::kVideoKind) {
        auto *video_track = static_cast<webrtc::VideoTrackInterface *>(track);
        mVideoSink = std::make_unique<VideoSink>(video_track);
    }
    track->Release();
}

void Controller::Data::OnRemoveTrack(rtc::scoped_refptr<webrtc::RtpReceiverInterface> receiver)
{
    receiver->track()->Release();
}

void Controller::Data::OnIceCandidate(const webrtc::IceCandidateInterface *candidate)
{
    using namespace rapidjson;

    log::logDebug("{0}: {1}", __func__, candidate->sdp_mline_index());

    rapidjson::Document d;
    d.SetObject();

    rapidjson::Document::AllocatorType &allocator = d.GetAllocator();

    d.AddMember(kSessionDescriptionTypeName, kCandidateSdpName, allocator);
    d.AddMember(kCandidateSdpMidName, Value(candidate->sdp_mid().c_str(), allocator), allocator);
    d.AddMember(kCandidateSdpMlineIndexName, candidate->sdp_mline_index(), allocator);

    std::string sdp;
    if (!candidate->ToString(&sdp)) {
        log::logError("{0}: Failed to serialize candidate", __func__);
        return;
    }
    d.AddMember(kCandidateSdpName, Value(sdp.c_str(), allocator), allocator);
    rapidjson::StringBuffer strbuf;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(strbuf);
    d.Accept(writer);

    PeerMessage message;
    message.peerId = mPeerId;
    message.data = sio::string_message::create(strbuf.GetString());
    mClient->SendToPeer(std::move(message));
}

void Controller::Data::OnSuccess(webrtc::SessionDescriptionInterface *desc)
{
    using namespace rapidjson;

    mPeerConnection->SetLocalDescription(
        DummySetSessionDescriptionObserver::Create(), desc);

    std::string sdp;
    desc->ToString(&sdp);

    rapidjson::Document d;
    d.SetObject();
    rapidjson::Document::AllocatorType &allocator = d.GetAllocator();
    d.AddMember(kSessionDescriptionTypeName, kSdpAnswerName, allocator);
    d.AddMember(kSessionDescriptionSdpName, Value(sdp.c_str(), allocator), allocator);
    rapidjson::StringBuffer strbuf;
    rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(strbuf);
    d.Accept(writer);

    PeerMessage message;
    message.peerId = mPeerId;
    message.data = sio::string_message::create(strbuf.GetString());
    mClient->SendToPeer(std::move(message));
}

void Controller::Data::OnFailure(webrtc::RTCError error)
{
    log::logError("{0}: {1}: {2}", __func__, ToString(error.type()), error.message());
}

// Interface implementation
Controller::Controller()
    : data{std::make_unique<Data>()}
{
}

Controller::~Controller()
{
}

void Controller::startStreaming()
{
    data->Connect(kServerUrl);
}

void Controller::stopStreaming()
{
    data->Disconnect();
}
