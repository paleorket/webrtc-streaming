#include "application.h"

#include <chrono>
#include <thread>

#include "controller.h"
#include "logger/logger.h"

namespace video_streamer
{
class Application::Data
{
public:
    Data()
        : mController{std::make_unique<Controller>()}
    {
        log::init(log::LogLevel::kDebug);
    }
    ~Data()
    {
        log::reset();
    }

    void start(std::function<bool()> &&checkExitFlag) noexcept
    {
        log::logInfo("Starting service ...");

        mController->startStreaming();

        while (checkExitFlag != nullptr && checkExitFlag() == false) {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        log::logInfo("Service is terminating ...");

        mController->stopStreaming();
    }

    std::unique_ptr<Controller> mController;
};
}   // namespace video_streamer

using namespace video_streamer;

Application::Application()
    : mData{std::make_unique<Data>()}
{}

Application::~Application() {}

void Application::run(std::function<bool()> &&checkExitFlag) noexcept
{
    mData->start(std::move(checkExitFlag));
}
