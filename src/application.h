#ifndef APPLICATION_H
#define APPLICATION_H

#include <functional>
#include <memory>

namespace video_streamer
{
class Application
{
public:
    Application();
    ~Application();

    Application(const Application &) = delete;
    Application &operator=(const Application &) = delete;
    Application(Application &&) noexcept = delete;
    Application &operator=(Application &&) noexcept = delete;

    void run(std::function<bool()> &&checkExitFlag) noexcept;

private:
    class Data;
    std::unique_ptr<Data> mData;
};

}   // namespace video_streamer

#endif   // APPLICATION_H
