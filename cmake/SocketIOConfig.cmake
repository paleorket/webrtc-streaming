set(SIO_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/thirdparty/sioclient/include")
set(SIO_LIBRARIES "${CMAKE_SOURCE_DIR}/thirdparty/sioclient/arm/lib/libsioclient.a")
message(STATUS "SocketIO found. Libraries: ${SIO_LIBRARIES}")
message(STATUS "SocketIO found. Headers: ${SIO_INCLUDE_DIR}")
