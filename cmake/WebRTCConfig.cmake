
set(WEBRTC_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/thirdparty/webrtc/include")
message(STATUS "WebRTC found. Headers: ${WEBRTC_INCLUDE_DIR}")
set(WEBRTC_LIBRARIES "${CMAKE_SOURCE_DIR}/thirdparty/webrtc/arm/lib/libwebrtc.a")
message(STATUS "WebRTC found. Libraries: ${WEBRTC_LIBRARIES}")
