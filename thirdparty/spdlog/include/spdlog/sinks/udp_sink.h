
//
// Copyright(c) 2019 IBS Project LLC http://ibsproject.kz
// Distributed under the MIT License (http://opensource.org/licenses/MIT)
//

#pragma once

#ifndef SPDLOG_H
#include "spdlog/spdlog.h"
#endif

#include "spdlog/details/null_mutex.h"
#include "spdlog/sinks/base_sink.h"

#include <mutex>
#include <string>

#include <boost/asio.hpp>
#include <boost/asio/ip/udp.hpp>

namespace spdlog {
namespace sinks {
/*
 * UDP Socket sink based on boost::asio
 */
template<typename Mutex>
class udp_sink final : public base_sink<Mutex>
{
public:
    explicit udp_sink(const std::string& ipv4_address, const std::string& service_name, uint16_t port) :
        service_name_(service_name),
        endpoint_(boost::asio::ip::address::from_string(ipv4_address), port),
        socket_(io_service_)
    {
        socket_.open(boost::asio::ip::udp::v4());
    }

    ~udp_sink() override {
        socket_.close();
    }

    udp_sink(const udp_sink&) = delete;
    udp_sink &operator=(const udp_sink&) = delete;

protected:
    void sink_it_(const details::log_msg &msg) override
    {
        std::string level = log_level(msg.level);

        std::stringstream ss;
        ss << level << "|"
           << service_name_ << "|"
           << msg.payload.data() << "|"
           << msg.thread_id;
        socket_.send_to(boost::asio::buffer(ss.str(), ss.str().size()), endpoint_);
    }
    void flush_() override {}

private:
    std::string log_level(const level::level_enum level) {
        std::string level_str;
        switch(level) {
            case level::level_enum::trace: level_str = "debug"; break;
            case level::level_enum::debug: level_str = "debug"; break;
            case level::level_enum::info: level_str = "info"; break;
            case level::level_enum::warn: level_str = "warning"; break;
            case level::level_enum::err: level_str = "critical"; break;
            default: level_str = "debug";
        }
        return level_str;
    }
    std::string service_name_;
    boost::asio::io_service io_service_{};
    boost::asio::ip::udp::endpoint endpoint_;
    boost::asio::ip::udp::socket socket_;
};

using udp_sink_mt = udp_sink<std::mutex>;
using udp_sink_st = udp_sink<details::null_mutex>;

} // namespace sinks

//
// factory functions
//

template<typename Factory = default_factory>
inline std::shared_ptr<logger> udp_logger_mt(
    const std::string &ipv4_address, const std::string& service_name, uint16_t port)
{
    return Factory::template create<sinks::udp_sink_mt>(ipv4_address, service_name, port);
}

template<typename Factory = default_factory>
inline std::shared_ptr<logger> udp_logger_st(
    const std::string &ipv4_address, const std::string& service_name, uint16_t port)
{
    return Factory::template create<sinks::udp_sink_st>(ipv4_address, service_name, port);
}

} // namespace spdlog
